#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use serialport::{available_ports};

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command

#[tauri::command]
fn list_devices() -> String {
    match available_ports() {
        Ok(ports) => {
            format!("{:?}", ports)
        }
        Err(e) => {
            format!("Error listing serial ports, {:?}", e)
        }
    }    
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![list_devices])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
