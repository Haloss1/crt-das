---
sidebar_position: 1
---

# Getting Started

## What's CRT-DAS?

CRT Digital alignment software (CRT-DAS) is a reverse engineering attempt for the WinDAS (Sony Windows Digital Alignment Software) serial protocol, right now focused on the `Sony CPD-G520P`, based on the `CR1` chassis.

Right now there isn't much, but take a look at the development guides.