---
sidebar_position: 2
---

# Send raw commands to the monitor

How to send raw commands to the monitor.

```
echo -en "\xf1\x07\x01\xf2\x03\x01\xef" > /home/USER/FILEFORYOURTTY
```

- for this step `/home/USER/FILEFORYOURTTY` can also just be your serial adaptor directly (`/dev/ttyUSB0`) if you don't need to get the response from the monitor