---
sidebar_position: 1
---

# Set up the sniffer

Setting up serial sniffing between your device and the monitor.

- Connect your serial adapter to both your PC and your Monitor, then:

```
interceptty /dev/ttyUSB0 /home/USER/FILEFORYOURTTY -v | interceptty-nicedump
```