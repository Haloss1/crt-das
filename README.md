# CRT-DAS (CRT Digital Alignment Software)

Reverse engineering the Windas (Sony Windows Digital Alignment Software) serial protocol, right now focused on the `Sony CPD-G520P`, based on the `CR1` chassis

## Getting started

### Set up the sniffer:

- Connect your serial adapter to both your PC and your Monitor, then:

```
interceptty /dev/ttyUSB0 /home/haloss1/xd -v | tee -a output.raw
```

Serial communication should be outputted to your console and to a file in your current directory named `output.raw`

### Send it to the monitor:

#### Single command:

```
echo -en "\xf1\x07\x01\xf2\x03\x01\xef" > /home/USER/FILEFORYOURTTY
```

- for this step `/home/USER/FILEFORYOURTTY` can also just be your serial adaptor directly (`/dev/ttyUSB0`) if you don't need to get the response from the monitor

#### Set of commands from a `.raw` file:

Edit [run-test.sh](dev-scripts/run-test.sh) and run it:

```
./run-test.sh
```

It should apply all of your commands got by the sniffer

## Commands found

| Command Name     | Value                                      | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Response              |
| ---------------- | ------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| Maintenance mode | `\0xf1\0x08\0x01\0xf0\0x01\0x01\0x02\0xee` | Puts monitor in some kind of maintenance mode, lets you run commands                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                       |
| Degauss          | `\0xf1\0x07\0x01\0xf2\0x03\0x01\0xef`      | Deagusses the monitor                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                       |
| Lock OSD         | `\0xf1\0x07\0x01\0x01\0x9f\0x20\0xb9`      | Locks the OSD, replicates the lock used when opening the procedure step window in windas                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |                       |
| Unlock OSD       | `\0xf1\0x07\0x01\0x01\0x9f\0x22\0xbb`      | Unlocks the OSD, replicates the unlock used when closing the procedure step window in windas or doing the finish procedure step                                                                                                                                                                                                                                                                                                                                                                                                                           |                       |
| Current G2 Value | `\0xf0\0x07\0x01\0x01\0x00\0x01\0xfa`      | Gets current G2 Value, the 3rd section from the response is the actual value                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | `0x50\0x04\0x00\0x54` |
| Set G2 Value     | `\0xf1\0x07\0x01\0x01\0x00\0x00\0xfa`      | This one is more complicated, to adjust the value you need to change the last two parts (`0x00` and `0xfa`), where `0x00` is the actual number, this one goes up to `255`, the other one is the `actual number - 6`, you can notice that at `0` this is actually `250`, when you reach higher values the second values resets to `0` (See [G2 Adjustment Interpretation](docs/adjustment/procedure/wpb/g2-adj.interpreted) for examples), both of this values should be equal to your target number, the second one with the calculation mentioned before |                       |
